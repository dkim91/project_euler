import java.math.BigInteger;

public class twelve {

    public static long divisor(long input) {

        BigInteger number = new BigInteger(String.valueOf(input));
        BigInteger x = new BigInteger("2");
        long totalFactors = 1;
        while (x.multiply(x).compareTo(number) <= 0) {
            int power = 0;
            while (number.mod(x).equals(BigInteger.ZERO)) {
                power++;
                number = number.divide(x);
            }
            totalFactors *= (power + 1);
            x = x.add(BigInteger.ONE);
        }
        if (!number.equals(BigInteger.ONE)) {
            totalFactors *= 2;
        }

        return totalFactors;
    }

    public static void main(String[] args) {
        long current = 0;
        long counter = 1;
        while (true) {
            current += counter;
            counter++;
            if (divisor(current) > 500) {
                System.out.println(current);
                return;
            }
        }
    }
}
