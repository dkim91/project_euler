import java.util.HashMap;

public class fourteen {

    //Trying out some dynammic programming??
    static HashMap<Long, Integer> prevAnswers = new HashMap<>();

    private static int collatz(long n) {
        if (n == 1) {
            return 1;
        }

        if (n % 2 == 0) {
            if (prevAnswers.containsKey(n / 2)) {
                return 1 + prevAnswers.get(n / 2);
            } else {
                int temp = collatz(n / 2);
                prevAnswers.put(n / 2, temp);
                return 1 + temp;
            }
        }

        // Odd number case
        else {
            if (prevAnswers.containsKey(3 * n + 1)) {
                return 1 + prevAnswers.get(3 * n + 1);
            } else {
                int temp = collatz(3 * n + 1);
                prevAnswers.put(3 * n + 1, temp);
                return 1 + temp;
            }
        }
    }

    public static void main(String[] args) {
        int max = 0;
        long position = 0;
        for (long i = 1; i <= 1000000; i++) {
            int steps = collatz(i);
            if (steps > max) {
                max = steps;
                position = i;
            }
        }
        System.out.println("position = " + position);
    }
}
